using System;
using System.Collections.Generic;
using CashRegisterDemo;
using Xunit;

namespace CashRegister.Tests
{
    public class CashRegisterTests
    {
        public static IEnumerable<Object[]> TestData
            {
                get
                { 
                    var item1 = new ItemByQuanity();
                    item1.name = "Cheerios";
                    item1.price = 10;
                    item1.quantity = 8;  // count
                    item1.DiscountType = DiscountType.Buy3Get1Free;

                    var item2 = new ItemByWeight();
                    item2.name = "Apples";
                    item2.price = 3;
                    item2.weight = 1.5; // Pounds
                    item2.DiscountType = DiscountType.NoDiscount;

                    var item3 = new ItemByQuanity();
                    item3.name = "Kellogs";
                    item3.price = 6;
                    item3.quantity = 1;  // count
                    item3.DiscountType = DiscountType.NoDiscount;
                  
                    var item4 = new ItemByWeight();
                    item4.name = "Oranges";
                    item4.price = 3;
                    item4.weight = 1.5; // Pounds
                    item4.DiscountType = DiscountType.NoDiscount;

                    var item5 = new ItemByQuanity();
                    item5.name = "FruitBars";
                    item5.price = 6;
                    item5.quantity = 0;  // count
                    item5.DiscountType = DiscountType.NoDiscount;

                    var item6 = new ItemByWeight();
                    item6.name = "Sugar";
                    item6.price = 3;
                    item6.weight = 1.5; // Pounds
                    item6.DiscountType = DiscountType.NoDiscount;

                    var item7 = new ItemByQuanity();
                    item7.name = "Snickers";
                    item7.price = 20;
                    item7.quantity = 5;  // count
                    item7.DiscountType = DiscountType.NoDiscount;

                    var item8 = new ItemByWeight();
                    item8.name = "Banana";
                    item8.price = 20;
                    item8.weight = 5; // Pounds
                    item8.DiscountType = DiscountType.NoDiscount;

                    ////Test Data with Items on Sale
                    List<Item> lsOfItemsInCart1 = new List<Item>() ;
                    lsOfItemsInCart1.Add(item1);
                    lsOfItemsInCart1.Add(item2);

                    ////Test Data with no Discount
                    List<Item> lsOfItemsInCart2 = new List<Item>();
                    lsOfItemsInCart2.Add(item3);
                    lsOfItemsInCart2.Add(item4);

                    ////Test Data with 0 value in one Item
                    List<Item> lsOfItemsInCart3 = new List<Item>();
                    lsOfItemsInCart3.Add(item5);
                    lsOfItemsInCart3.Add(item6);

                    ////Test Data which is eligible for discount on final price
                    List<Item> lsOfItemsInCart4 = new List<Item>();
                    lsOfItemsInCart4.Add(item7);
                    lsOfItemsInCart4.Add(item8);

                    ////Test Data with 4 items 
                    List<Item> lsOfItemsInCart5 = new List<Item>();
                    lsOfItemsInCart5.Add(item3);
                    lsOfItemsInCart5.Add(item4);
                    lsOfItemsInCart5.Add(item1);
                    lsOfItemsInCart5.Add(item2);

                    ////Test Data with 4 items and applicable for discount 
                    List<Item> lsOfItemsInCart6 = new List<Item>();
                    lsOfItemsInCart6.Add(item7);
                    lsOfItemsInCart6.Add(item8);
                    lsOfItemsInCart6.Add(item5);
                    lsOfItemsInCart6.Add(item6);

                yield return new object[] { lsOfItemsInCart1, 64.5 };
                yield return new object[] { lsOfItemsInCart2, 10.5};
                yield return new object[] { lsOfItemsInCart3, 4.5 };
                //this is expected to fail
                yield return new object[] { lsOfItemsInCart4, 4.5 }; 
                yield return new object[] { lsOfItemsInCart4, 190 };
                yield return new object[] { lsOfItemsInCart5, 75 };
                yield return new object[] { lsOfItemsInCart6, 194.27 };

            }

        }

        [Theory]
        [MemberData("TestData")]
        public void TestCalculateBillWithItemsInCart(List<Item> items, double expectedValue)
        {
            var sc = new ShoppingCart();
            if (items != null)
            {
                foreach (Item i in items)
                {
                    sc.items.Add(i);
                }
            }
            double result = sc.CalculateBill();
            Assert.Equal(expectedValue, result,1);
        }
    }
}
