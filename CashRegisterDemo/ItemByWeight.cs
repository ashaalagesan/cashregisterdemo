﻿using System;
namespace CashRegisterDemo
{
    public class ItemByWeight : Item
    {
        public ItemByWeight()
        {
        }
        double _weight = 0;

        public double weight
        {
            get { return _weight; }
            set { _weight = value; }
        }


        public override double Calculate()
        {
            double calculatedPrice = this.price * this.weight;
            return calculatedPrice;
        }
    }
}
