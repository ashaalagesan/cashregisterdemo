﻿using System;
using System.Collections.Generic;

namespace CashRegisterDemo
{
    public class ShoppingCart
    {
        List<Item> _items;
        private double  discount = 5; // 5$ off 100$ expressed as  a percentage 
        public ShoppingCart()
        {
            items = new List<Item>();

        }

        public List<Item> items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
            }
        }

        //Calculate final bill with items in cart
        public double CalculateBill()
        {

            double price = 0;
            if (this.items != null)
            {

                foreach (Item i in this.items)
                {

                    price += i.Calculate();
                }

            }

            //Apply  discount
            if (price >= 100)
            {
                price = price - price * (discount / 100);
            }

            return price;

        }
    }
}
