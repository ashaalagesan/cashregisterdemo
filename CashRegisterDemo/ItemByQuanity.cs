﻿using System;
namespace CashRegisterDemo
{
    public class ItemByQuanity : Item
    {
        public ItemByQuanity()
        {
        }

        int _quantity = 0;
        double calculatedPrice = 0;
        double discountedPrice = 0;
        public int quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        public override double Calculate()
        {
             
            if (this.DiscountType  == DiscountType.Buy3Get1Free && this.quantity > 3 )
            {
                // Applying Buy 3 get 1 free offer as a percentage
                discountedPrice = 0.25 * (this.price * this.quantity);
                calculatedPrice = (this.price * this.quantity)- discountedPrice;
            }
            else
            {
                calculatedPrice = (this.price * this.quantity);
            }

            return calculatedPrice;
        }
    }
}
