﻿namespace CashRegisterDemo
{
    public enum DiscountType { NoDiscount, Buy3Get1Free };
    public abstract class Item
    {

        string _name = "";
        double _price = 0;
        DiscountType _discountType;

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        public double price
        {
            get { return _price; }
            set { _price = value; }
        }

       
        public DiscountType DiscountType
        {
          
            get { return _discountType; }
            set { _discountType = value; }
        }

        public abstract double Calculate();
    }



}